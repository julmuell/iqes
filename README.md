Bachelorthesis / Project IQES
=============================

This is the git repository of the prototype IQES (which means "Intelligent Query Expansion System"). The prototype has been developed within the thesis:

> Query Expansion auf Basis von Semantischen Netzen und Geografischen Informationen

Usage of IQES
-------------

IQES offers a well defined REST Interface with two main methods:

+ GET /expand/:query[?config=:configAsJSON]
+ POST /expand

## Configuration as JSON
```javascript
{
	// Should we execute the GeoNames Expansion
	isExpansionByGeoNamesActive : true,
	// Should we execute the GermaNet Expansion
	isExpansionByGermaNetActive : true,
	// Should we execute the DBpedia Expansion
	isExpansionByLODActive : true,
	
	// The radius of the GeoNames expansion
	expansionRadiusForGeoNames : 5,
	// The result limit of the GeoNames expansion
	maxExpansionRowsForGeoNames : 20,
	// Priority order of the initial GeoNames results
	priorityOrderForGeoNamesMatching : [
		"A",
		"P"
	],
	// Preprocessing on/off components
	whichPRShouldrun : {
		reset : true ,
		tokeniser : true ,
		splitter : true ,
		Max Ent POS Tagger : true ,
		compound analysis gazetteer : true ,
		orthomatcher : true ,
		german gazetteer : true ,
		german grammar : true ,
		German Stemmer : true ,
		Musik Genre Gazetteer : true
	}
}
```

## POST key-value-pairs

query: "query"
configuration.isExpansionByGermaNetActive: true  
configuration.isExpansionByLODActive: true  
configuration.isExpansionByGeoNamesActive: true  
configuration.expansionRadiusForGeoNames: 5  
configuration.maxExpansionRowsForGeoNames: 20  
configuration.priorityOrderForGeoNamesMatching[0]: "A"  
configuration.priorityOrderForGeoNamesMatching[1]: "P"  
configuration.whichPRShouldrun[tokeniser]: true  
configuration.whichPRShouldrun[splitter]: true  
configuration.whichPRShouldrun[Max Ent POS Tagger]: true  
configuration.whichPRShouldrun[german gazetteer]: true  
configuration.whichPRShouldrun[german grammar]: true  
configuration.whichPRShouldrun[German Stemmer]: true  
configuration.whichPRShouldrun[Lupedia Lookup]: true  
configuration.whichPRShouldrun[Musik Genre Gazetteer]: true  

### You can find more informations about the entire project in the file "IQES-Thesis.pdf".
