import static org.fest.assertions.Assertions.assertThat;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

import org.junit.Test;

import play.libs.WS;

public class IntegrationTest {

	/**
	 * add your integration test here in this example we just check if the
	 * welcome page is being shown
	 */
	@Test
	public void test() {
		running(testServer(3333), new Runnable() {
			public void run() {
				assertThat(
						WS.url("http://localhost:3333").get().get(5010)
								.getStatus()).isEqualTo(OK);
				
				System.out.println(WS.url("http://localhost:3333").get().get(5010).getBody());
			}
		});
	}

}
