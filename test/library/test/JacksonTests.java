package library.test;
import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.UUID;

import org.junit.Test;
import static org.junit.Assert.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;
import de.htwg.iqes.queryexpansion.domain.impl.QueryExpansionSetImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryTermImpl;

public class JacksonTests {

	@Test
	public void serializeQueryWithMixIn_expectAsConfigured() throws JsonProcessingException {
		// GIVEN
		List<QueryExpansionSet> listQueryExpansionSets = Lists.newArrayList();

		listQueryExpansionSets.add(new QueryExpansionSetImpl(
				"expansionModuleName", LinkedListMultimap
						.<String, String> create()));

		List<QueryTerm> listQueryTerms = Lists.newArrayList();

		listQueryTerms
				.add(new QueryTermImpl(UUID.randomUUID(), "Query",
						"queryAlternate", "unknownType", Maps
								.<String, String> newHashMap(),
						listQueryExpansionSets));

		Query query = new QueryImpl("This is a Query",
				Lists.newArrayList(listQueryTerms));

		ObjectMapper mapper = new ObjectMapper();
		mapper.addMixInAnnotations(QueryTermImpl.class,
				MixInToIgnoreProperties.class);
		
		// WHEN
		String mapped = mapper.writeValueAsString(query);

		// THEN
		assertFalse(mapped.contains("queryAlternate"));
		assertTrue(mapped.contains("newFancyName"));
		
	}

	static abstract class MixInToIgnoreProperties {
		@JsonIgnore
		abstract String getAlternateForm();
		
		@JsonProperty("newFancyName")
		abstract String getTerm();
	}
}
