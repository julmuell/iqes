package library.test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

public class MultimapTests {

	@Test
	public void fillMultimapWithOneKeyAndStrings_expectSavedStringsWithinSameKey() {
		// GIVEN
		Multimap<String, String> test = LinkedListMultimap.create();

		// WHEN
		test.put("test1", "test1.1");
		test.put("test1", "test1.2");
		test.put("test1", "test1.3");
		test.put("test1", "test1.4");
		test.put("test1", "test1.5");

		// THEN
		assertTrue(test.get("test1").contains("test1.1"));
		assertTrue(test.get("test1").contains("test1.5"));
	}

	@Test
	public void testQuery() throws JsonProcessingException {
		// GIVEN
		ObjectMapper mapper = new ObjectMapper();
		Multimap<String, String> test = LinkedListMultimap.create();

		// WHEN
		test.put("test1", "test1.1");
		test.put("test1", "test1.2");
		test.put("test1", "test1.3");
		test.put("test1", "test1.4");
		test.put("test1", "test1.5");

		// THEN
		assertFalse(mapper.writeValueAsString(test).contains("test1"));
	}
	
	@Test
	public void testQuery2() throws JsonProcessingException {
		// GIVEN
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new GuavaModule());
		Multimap<String, String> test = LinkedListMultimap.create();

		// WHEN
		test.put("test1", "test1.1");
		test.put("test1", "test1.2");
		test.put("test1", "test1.3");
		test.put("test1", "test1.4");
		test.put("test1", "test1.5");

		// THEN
		assertTrue(mapper.writeValueAsString(test).contains("test1"));
	}
}
