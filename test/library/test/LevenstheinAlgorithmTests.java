package library.test;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinRouter;

import com.google.common.base.Stopwatch;

public class LevenstheinAlgorithmTests {

	public static class Testi extends UntypedActor {

		@Override
		public void onReceive(Object arg0) throws Exception {
			System.out.println("huhuuu");
		}

	}

	@Test
	public void test() {

		ActorSystem system = ActorSystem.create("MegaActor");

		ActorRef ref = system.actorOf(Props.create(Testi.class).withRouter(
				new RoundRobinRouter(10)));

		for (long i = 0; i < 100L; i++) {
			ref.tell(new LevenstheinAlgorithmTests(), null);
		}

		List<String> arr = new ArrayList<>();

		for (int i = 0; i < 100750; i++) {
			arr.add("abcdevoelfös");
		}

		final String toCompare = "sdlkfm";

		Stopwatch watch = Stopwatch.createStarted();

		for (String x : arr) {
			StringUtils.getLevenshteinDistance(x, toCompare);
		}

		System.out.println(watch.elapsed(TimeUnit.MILLISECONDS));
	}
}
