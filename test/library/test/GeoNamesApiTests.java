package library.test;
import java.util.concurrent.TimeUnit;

import org.geonames.FeatureClass;
import org.geonames.Toponym;
import org.geonames.ToponymSearchCriteria;
import org.geonames.ToponymSearchResult;
import org.geonames.WebService;
import org.junit.Test;

import com.google.common.base.Stopwatch;


public class GeoNamesApiTests {

	private static final String USERNAME = "julmuell";

	@Test
	public void testSearch(){
		WebService.setUserName(USERNAME);
		
		ToponymSearchCriteria search = new ToponymSearchCriteria();
		search.setName("Bühlerzell");
		search.setFeatureClass(FeatureClass.A);
		search.setMaxRows(1);
		
		Stopwatch watch = Stopwatch.createStarted();
		try {
			ToponymSearchResult result = WebService.search(search);
			
			for(Toponym toponym : result.getToponyms()) {
				System.out.println(toponym.getName() + " >> " + toponym.getFeatureClass());
				
				switch( toponym.getFeatureClass()) {
				case P: // Place
				case H:
				case T: // Mountain
					for(Toponym x : WebService.findNearby(toponym.getLatitude(), toponym.getLongitude(), 7.5, FeatureClass.P, null, null, 6500)){
						System.out.println(x.getName());
					}
					break;
				case L:
				case A:
					for(Toponym x : WebService.children(toponym.getGeoNameId(), null, null).getToponyms()){
						System.out.println(x.getName());
					}
					break;
				
				default: break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(watch.elapsed(TimeUnit.MILLISECONDS));
	}
}

