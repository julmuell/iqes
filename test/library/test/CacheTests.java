package library.test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.google.common.base.Stopwatch;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class CacheTests {
	@Test
	public void testGuavaCache() throws ExecutionException {
		LoadingCache<String, String> cache = CacheBuilder.newBuilder().build(
				new CacheLoader<String, String>() {

					@Override
					public String load(String arg0) throws Exception {
						Thread.sleep(500);
						return "test return";
					}

				});

		Stopwatch stop = Stopwatch.createStarted();

		System.out.println(cache.get("test"));

		System.out.println(stop.elapsed(TimeUnit.MILLISECONDS));

		stop.reset().start();

		System.out.println(cache.get("test"));
		System.out.println(cache.get("test"));

		System.out.println(stop.elapsed(TimeUnit.MILLISECONDS));

	}
}
