package library.test;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.Test;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;

public class RangeTests {
	@Test
	public void blub() throws UnsupportedEncodingException {
		List<Range<Integer>> lng = Lists.newArrayList();
		lng.add(Range.closed(0, 5));
		lng.add(Range.closed(6, 10));
		lng.add(Range.closed(11, 13));
		
		final Range<Integer> test = Range.closed(0, 10);
		
		System.out.println(Iterables.filter(lng, new Predicate<Range<Integer>>() {
			@Override
			public boolean apply(Range<Integer> input) {
				return ! test.encloses(input);
			}
		}));

	}
}
