package de.htwg.iqes.queryexpansion.dbpedia;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.queryexpansion.QueryExpansion;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;
import de.htwg.iqes.queryexpansion.domain.impl.QueryImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryTermImpl;

public class DBpediaTests {

	@Test
	@Ignore
	public void testMusicSparl() throws JsonProcessingException {
		QueryExpansion exp = new DBpediaQE();
		Configuration config = Mockito.mock(Configuration.class);
		Mockito.when(config.isExpansionByLODActive()).thenReturn(true);
		exp.setConfiguration(config);

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new GuavaModule());
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		Map<String, String> map = Maps.<String, String> newHashMap();
		map.put("inst", "http://dbpedia.org/resource/Rock_music");

		List<QueryTerm> terms = Lists.newArrayList();
		terms.add(new QueryTermImpl(UUID.randomUUID(), "BMW", "bmw", "Company",
				map, Lists.<QueryExpansionSet> newArrayList()));

		System.out.println(mapper.writeValueAsString(exp
				.expandQuery(new QueryImpl(
						"http://dbpedia.org/resource/Rock_music", terms))));
	}
}
