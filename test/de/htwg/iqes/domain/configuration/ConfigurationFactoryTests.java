package de.htwg.iqes.domain.configuration;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openqa.selenium.android.library.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;

import de.htwg.iqes.domain.configuration.impl.ConfigurationFactoryImpl;

public class ConfigurationFactoryTests {

	@Mock
	Configuration mockConfig;

	ObjectMapper objectMapper;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		objectMapper = new ObjectMapper();
	}

	@Test(expected = NullPointerException.class)
	public void createConfigurationWithFactoryAndNullAsJsonString_expectNullPointerException() {
		// GIVEN
		ConfigurationFactory factory = createConfigurationFactoryImpl();

		// WHEN
		factory.createWithJson(null);

		// THEN
	}

	private ConfigurationFactoryImpl createConfigurationFactoryImpl() {
		return new ConfigurationFactoryImpl(mockConfig, objectMapper,
				Logger.getLogger());
	}

	@Test
	public void createConfigurationWithFactoryAndEmptyString_expectDefaultConfiguration() {
		// GIVEN
		ConfigurationFactory factory = createConfigurationFactoryImpl();
		String emptyString = "";

		// WHEN
		Configuration configurationByFactory = factory.createWithJson(Optional
				.of(emptyString));

		// THEN
		assertTrue(configurationByFactory.equals(mockConfig));
		assertTrue(configurationByFactory == mockConfig); // Same Object
	}

	@Test
	public void createConfigurationWithFactoryAndFaultString_expectDefaultConfiguration() {
		// GIVEN
		ConfigurationFactory factory = createConfigurationFactoryImpl();
		String faultString = "{ \"test\" : \"test\"}";

		// WHEN
		Configuration configurationByFactory = factory.createWithJson(Optional
				.of(faultString));

		// THEN
		assertTrue(configurationByFactory.equals(mockConfig));
		assertTrue(configurationByFactory == mockConfig); // Same Object
	}

	@Test
	public void createConfigurationWithFactoryAndFullJsonConfiguration_expectNewlyLoadedConfiguration()
			throws URISyntaxException, IOException {
		// GIVEN
		ConfigurationFactory factory = createConfigurationFactoryImpl();
		String correctJsonString = FileUtils.readFileToString(FileUtils
				.toFile(this.getClass().getClassLoader()
						.getResource("configuration.txt")));

		// WHEN
		Configuration configurationByFactory = factory.createWithJson(Optional
				.of(correctJsonString));

		// THEN
		assertTrue(configurationByFactory != mockConfig); // Same Object
		assertTrue(configurationByFactory.getExpansionRadiusForGeoNames() == 5);
	}
}
