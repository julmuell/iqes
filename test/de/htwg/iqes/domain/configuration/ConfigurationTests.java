package de.htwg.iqes.domain.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;

import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.domain.configuration.impl.ConfigurationImpl;

public class ConfigurationTests {

	@Test
	public void testConfiguraiton() throws JsonParseException,
			JsonMappingException, IOException {
		StringWriter writer = new StringWriter();
		InputStream in = ConfigurationTests.class
				.getResourceAsStream("/defaultIQESConfiguration.txt");

		try {
			IOUtils.copy(in, writer, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new GuavaModule());

		Configuration config = mapper.readValue(writer.toString(),
				ConfigurationImpl.class);
		System.out.println(config.getMaxExpansionRowsForGeoNames());

		System.out.println(mapper.writeValueAsString(config));
	}
}
