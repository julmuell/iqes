package de.htwg.iqes.thesis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mindswap.pellet.jena.PelletReasonerFactory;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class MamaPapa {
	@Test
	public void sparlQuery() throws IOException {
		File mamaPapaOwl = FileUtils.toFile(this.getClass().getClassLoader()
				.getResource("mamapapa.owl"));
		String graphURI = "http://www.semanticweb.org/jules-lap/ontologies/2013/10/untitled-ontology-24#";

		// create a model using reasoner
		OntModel model1 = ModelFactory.createOntologyModel(PelletReasonerFactory.THE_SPEC);

		// create a model which doesn't use a reasoner
		OntModel model2 = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);

		// read the RDF/XML file
		InputStream in = new FileInputStream(mamaPapaOwl);
		InputStream in2 = new FileInputStream(mamaPapaOwl);

		model1.read(in, graphURI);
		model2.read(in2, graphURI);
		// model2.read(SOURCE, "RDF/XML");

		// Create a new query
		String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX ex: <http://www.semanticweb.org/jules-lap/ontologies/2013/10/untitled-ontology-24#>"
				+ "SELECT * WHERE " + "{" + "	?mama rdf:type ex:Mutter;" + "			rdf:type ?types;"
				+ "			ex:name ?name;" + "			ex:hatKind ?kind." + "}";

		System.out.println(queryString);

		Query query = QueryFactory.create(queryString);

		System.out.println("----------------------");
		System.out.println("Query Result Sheet");
		System.out.println("----------------------");

		System.out.println("Direct&Indirect Descendants (model1)");
		System.out.println("-------------------");

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, model1);
		com.hp.hpl.jena.query.ResultSet results = qe.execSelect();

		// Output query results
		ResultSetFormatter.out(System.out, results, query);

		qe.close();

		System.out.println("----------------------");
		System.out.println("Only Direct Descendants");
		System.out.println("----------------------");

		// Execute the query and obtain results
		qe = QueryExecutionFactory.create(query, model2);
		results = qe.execSelect();

		// Output query results
		ResultSetFormatter.out(System.out, results, query);
		qe.close();
	}
}
