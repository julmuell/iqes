package de.htwg.iqes.thesis.evaluation;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.stub;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;

import org.geonames.FeatureClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.Application;
import play.api.DefaultApplication;
import play.api.Mode;
import play.api.Play;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import de.htwg.iqes.controller.IQESController;
import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.domain.configuration.ConfigurationFactory;
import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;

/**
 * Evaluates the precision and recall of the Query Expansion System.
 * 
 * @author "Julian Müller"
 * 
 */
public class EvaluationIQES {

	private static Application app;

	private IQESController ctrl;

	private ConfigurationFactory fac;

	@BeforeClass
	public static void initIQES() {
		app = new Application(new DefaultApplication(new File("."),
				EvaluationIQES.class.getClassLoader(), null, Mode.Dev()));
		Play.start(app.getWrappedApplication());
	}

	@AfterClass
	public static void shutdownIQES() {
		Play.stop();
	}

	@Before
	public void initController() {
		ctrl = app.getWrappedApplication().global()
				.getControllerInstance(IQESController.class);
		fac = app.getWrappedApplication().global()
				.getControllerInstance(ConfigurationFactory.class);
	}

	private List<String> expansionTermsFlat(Query q) {
		List<String> terms = Lists.newLinkedList();

		for (QueryTerm term : q.getQueryTerms()) {
			for (QueryExpansionSet set : term.getExpansionSets()) {
				terms.addAll(set.getTermsByFeature().values());
			}
		}

		return terms;
	}

	@Test
	public void test() throws FileNotFoundException, URISyntaxException,
			JsonProcessingException {
		// Hint: without .toURI() -> Fail.
		File folder = new File(this.getClass().getClassLoader()
				.getResource("resources/evaluation/").toURI());
		Multimap<String, String> queryAndExpectations = LinkedHashMultimap
				.create();

		for (File f : folder.listFiles()) {
			System.out.println(f.toString());
			addQueryAndExpectationsToMultimap(queryAndExpectations, f);
		}

		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.registerModule(new GuavaModule());
		System.out.println(mapper.writeValueAsString(queryAndExpectations));

		int i = 0;
		double sumRecall = 0.0;
		double sumPrecision = 0.0;
		double sumF1 = 0.0;
		double sumF05 = 0.0;
		double sumF2 = 0.0;

		for (Entry<String, Collection<String>> toTest : queryAndExpectations
				.asMap().entrySet()) {
			Configuration config = fac.get();
			Configuration configSpy = spy(config);

			stub(configSpy.getPriorityOrderForGeoNamesMatching()).toReturn(
					ImmutableSet.<FeatureClass> builder().add(FeatureClass.P)
							.addAll(Arrays.asList(FeatureClass.values()))
							.build());

			final String key = toTest.getKey();

			Query q = ctrl.expandQuery(key, configSpy);
			Collection<String> should = toTest.getValue();
			List<String> got = expansionTermsFlat(q);

			int howmany = 0;

			for (String s : should) {
				if (got.contains(s))
					howmany++;
			}

			Evaluation eva = new Evaluation(got.size(), howmany, should.size());

			sumRecall += eva.recall();
			sumPrecision += eva.precision();
			sumF1 += eva.f1();
			sumF05 += eva.f05();
			sumF2 += eva.f2();

			System.out.println("Recall: " + eva.recall());
			System.out.println("Precision: " + eva.precision());
			System.out.println("F1: " + eva.f1());
			System.out.println("F0.5: " + eva.f05());
			System.out.println("F2: " + eva.f2());

			i++;
		}

		System.out.println("Sum Recall " + sumRecall);
		System.out.println("Average Recall " + sumRecall / i);
		System.out.println("Sum Precision " + sumPrecision);
		System.out.println("Average Precision " + sumPrecision / i);
		System.out.println("Sum F1 " + sumF1);
		System.out.println("Average F1 " + sumF1 / i);
		System.out.println("Sum F05 " + sumF05);
		System.out.println("Average F05 " + sumF05 / i);
		System.out.println("Sum F2 " + sumF2);
		System.out.println("Average F2 " + sumF2 / i);

	}

	private void addQueryAndExpectationsToMultimap(
			Multimap<String, String> map, File f) {
		Scanner scanner = null;

		try {
			scanner = new Scanner(f);
			String q = scanner.nextLine();
			scanner.nextLine();

			while (scanner.hasNextLine()) {
				map.put(q, scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
		} finally {
			if (scanner != null)
				scanner.close();
		}

	}

	private static final class Evaluation {

		private final int searchResultQuantity;

		private final int relevantSearchResultQuantity;

		private final int relevantDocumentsQuantity;

		public Evaluation(int searchResultQuantity,
				int relevantSearchResultQuantity, int relevantDocumentsQuantity) {
			System.out.println("How many results? " + searchResultQuantity
					+ "\nHow many 'r relevant? " + relevantSearchResultQuantity
					+ "\nHow many could be relevant? "
					+ relevantDocumentsQuantity);

			this.searchResultQuantity = searchResultQuantity;
			this.relevantSearchResultQuantity = relevantSearchResultQuantity;
			this.relevantDocumentsQuantity = relevantDocumentsQuantity;
		}

		public double recall() {
			return (double) relevantSearchResultQuantity
					/ relevantDocumentsQuantity;
		}

		public double precision() {
			return (double) relevantSearchResultQuantity / searchResultQuantity;
		}

		public double f1() {
			return fß(1.0);
		}

		public double fß(double ß) {
			if (precision() + recall() == 0.0)
				return 0.0;

			return (1.0 + (ß * ß))
					* ((precision() * recall()) / (ß * ß * precision() + recall()));
		}

		public double f2() {
			return fß(2.0);
		}

		public double f05() {
			return fß(0.5);
		}
	}
}
