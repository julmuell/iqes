package de.htwg.iqes.queryexpansion.domain;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The query term which is necessary for all expansion modules.
 * 
 * @author "Julian Müller"
 *
 */
public interface QueryTerm {

	@JsonIgnore
	public UUID getID();
	
	public String getTerm();

	public String getAlternateForm();

	public String getType();
	
	public Map<String, String> getFeatures();

	public List<QueryExpansionSet> getExpansionSets();

}