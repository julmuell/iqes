package de.htwg.iqes.queryexpansion.domain;

import java.util.List;
import java.util.UUID;

import com.google.common.base.Predicate;

/**
 * A query object is the main object within IQES. It holds the expansionjob and
 * also the processed query.
 * 
 * @author "Julian Müller"
 * 
 */
public interface Query {

	public String getOriginalQuery();

	public List<QueryTerm> getQueryTerms();

	public QueryTerm getQueryTermWithID(UUID id);

	public List<QueryTerm> getQueryTermsWithType(String... type);

	public List<QueryTerm> getQueryTermsFilteredByPredicate(
			Predicate<QueryTerm> pred);

	public Query mergeWith(Query... query);
}