package de.htwg.iqes.queryexpansion.domain;

import com.google.common.collect.Multimap;

/**
 * The expansion set of a specific module.
 * 
 * @author "Julian Müller"
 *
 */
public interface QueryExpansionSet {

	public String getName();

	public Multimap<String, String> getTermsByFeature();

}