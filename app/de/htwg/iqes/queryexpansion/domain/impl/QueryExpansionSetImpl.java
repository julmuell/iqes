package de.htwg.iqes.queryexpansion.domain.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;

public final class QueryExpansionSetImpl implements QueryExpansionSet {
	private final String name;

	private final ImmutableMultimap<String, String> termsByFeature;

	public QueryExpansionSetImpl(@JsonProperty("name") String name,
			@JsonProperty("termsByFeature") Multimap<String, String> terms) {
		this.name = name;
		termsByFeature = ImmutableMultimap.copyOf(terms);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Multimap<String, String> getTermsByFeature() {
		return termsByFeature;
	}
}