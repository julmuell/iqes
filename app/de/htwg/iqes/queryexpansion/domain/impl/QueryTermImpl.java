package de.htwg.iqes.queryexpansion.domain.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;

public final class QueryTermImpl implements QueryTerm {
	private final UUID id;
	
	private final String term;

	private final String alternateForm;

	private final String type;
	
	private final Map<String, String> features;

	private final ImmutableList<QueryExpansionSet> expansionSets;

	public QueryTermImpl(@JsonProperty("id") UUID id,
			@JsonProperty("term") String term,
			@JsonProperty("alternateForm") String alternateForm, @JsonProperty("type") String type,
			@JsonProperty("features") Map<String, String> features,
			@JsonProperty("expansionSets") List<QueryExpansionSet> expansionSets) {
		this.id = id;
		this.term = term;
		this.alternateForm = alternateForm;
		this.type = type;
		this.features = features;
		this.expansionSets = ImmutableList.copyOf(expansionSets);
	}

	@Override
	public UUID getID() {
		return id;
	}
	
	@Override
	public String getTerm() {
		return term;
	}

	@Override
	public String getAlternateForm() {
		return alternateForm;
	}

	@Override
	public String getType() {
		return type;
	}

	public Map<String, String> getFeatures() {
		return features;
	}

	@Override
	public List<QueryExpansionSet> getExpansionSets() {
		return expansionSets;
	}
}