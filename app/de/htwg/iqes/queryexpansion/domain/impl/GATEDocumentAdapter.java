package de.htwg.iqes.queryexpansion.domain.impl;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.annotation.AnnotationSetImpl;
import gate.creole.ANNIEConstants;
import gate.util.InvalidOffsetException;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Queues;
import com.google.common.collect.Range;

import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;

/**
 * The GateDocumentAdapter is mapping GATE Documents to IQES Documents.
 * 
 * @author "Julian Müller"
 *
 */
public class GATEDocumentAdapter implements Query {

	private static final String STOP_MAJOR_TYPE = "stop";
	private static final String MAJOR_TYPE_NAME = "majorType";
	private static final String STEM_FEATURE_NAME = "stem";
	private static final String FILTER_SENTENCE_TYPE = "Sentence";
	private final Query query;

	public GATEDocumentAdapter(Document doc) throws InvalidOffsetException {
		String content = doc.getContent().toString();
		AnnotationSet defaultAnnotationSet = doc.getAnnotations();
		AnnotationSet tokens = defaultAnnotationSet
				.get(ANNIEConstants.TOKEN_ANNOTATION_TYPE);

		List<Range<Long>> entiretokenRanges = collectTokenRanges(tokens);

		List<Annotation> save = Lists.newArrayList();

		Queue<Range<Long>> tokensToRead = Queues
				.newArrayDeque(entiretokenRanges);

		while (!tokensToRead.isEmpty()) {
			Range<Long> currentToken = tokensToRead.poll();
			AnnotationSet annotationSetWithSameStartNode = defaultAnnotationSet
					.get(currentToken.lowerEndpoint());

			List<Annotation> filterAndOrderedAS = filterAndOrderAnnotationSet(annotationSetWithSameStartNode);

			final Annotation firstAnnotation = filterAndOrderedAS.get(0);

			save.add(firstAnnotation);

			removeEnclosedRangesFromQueue(entiretokenRanges, tokensToRead,
					firstAnnotation);
		}

		List<QueryTerm> queryTermsToSave = Lists.newArrayList();

		for (Annotation toSave : save) {
			createQueryTermAndAddToList(doc, queryTermsToSave, toSave);
		}

		this.query = new QueryImpl(content, queryTermsToSave);
	}

	private void createQueryTermAndAddToList(Document doc,
			List<QueryTerm> termsToSave, Annotation toSave)
			throws InvalidOffsetException {
		if (STOP_MAJOR_TYPE.equals(toSave.getFeatures().get(MAJOR_TYPE_NAME)))
			return; // no stop words...

		AnnotationSet tokenSet = doc.getAnnotations().get(
				ANNIEConstants.TOKEN_ANNOTATION_TYPE);

		String termText = doc
				.getContent()
				.getContent(toSave.getStartNode().getOffset(),
						toSave.getEndNode().getOffset()).toString();

		Optional<String> alternateForm = Optional.absent();

		Map<String, String> features = Maps.newHashMap();

		Optional<Annotation> token = getAnnotationOfTypeToken(toSave, tokenSet);

		if (token.isPresent()
				&& token.get().getStartNode().equals(toSave.getStartNode())
				&& token.get().getEndNode().equals(toSave.getEndNode())) {
			alternateForm = Optional.fromNullable((String) token.get()
					.getFeatures().get(STEM_FEATURE_NAME));

			for (Map.Entry<Object, Object> x : token.get().getFeatures()
					.entrySet()) {
				features.put(String.valueOf(x.getKey()),
						String.valueOf(x.getValue()));
			}
		}

		for (Map.Entry<Object, Object> x : toSave.getFeatures().entrySet()) {
			features.put(String.valueOf(x.getKey()),
					String.valueOf(x.getValue()));
		}

		QueryTerm term = new QueryTermImpl(UUID.randomUUID(), termText,
				alternateForm.or(""), toSave.getType(), features,
				Lists.<QueryExpansionSet> newArrayList());

		termsToSave.add(term);
	}

	private Optional<Annotation> getAnnotationOfTypeToken(Annotation toSave,
			AnnotationSet tokenSet) {
		Iterator<Annotation> token = ((AnnotationSetImpl) tokenSet).getStrict(
				toSave.getStartNode().getOffset(),
				toSave.getEndNode().getOffset()).iterator();
		if (token.hasNext())
			return Optional.of(token.next());

		return Optional.absent();
	}

	private void removeEnclosedRangesFromQueue(List<Range<Long>> tokenRanges,
			Queue<Range<Long>> tokensToRead, final Annotation firstAnnotation) {
		tokensToRead.removeAll(Lists.newArrayList(Iterables.filter(tokenRanges,
				new Predicate<Range<Long>>() {
					@Override
					public boolean apply(Range<Long> input) {
						return Range.closed(
								firstAnnotation.getStartNode().getOffset(),
								firstAnnotation.getEndNode().getOffset())
								.encloses(input);
					}
				})));
	}

	private List<Annotation> filterAndOrderAnnotationSet(
			AnnotationSet annotationSetWithSameStartNode) {
		Ordering<Annotation> ordering = Ordering
				.from(new Comparator<Annotation>() {
					@Override
					public int compare(Annotation o1, Annotation o2) {
						return o1.getEndNode().getOffset()
								.compareTo(o2.getEndNode().getOffset());
					}
				}).compound(new Comparator<Annotation>() {
					@Override
					public int compare(Annotation o1, Annotation o2) {
						return o1.getId().compareTo(o2.getId());
					}
				}).reverse();

		List<Annotation> currentAnnotation = Lists.newArrayList(Iterables
				.filter(annotationSetWithSameStartNode,
						new Predicate<Annotation>() {
							@Override
							public boolean apply(Annotation input) {
								return !FILTER_SENTENCE_TYPE.equals(input
										.getType());
							}
						}));

		Collections.sort(currentAnnotation, ordering);
		return currentAnnotation;
	}

	private List<Range<Long>> collectTokenRanges(AnnotationSet tokens) {
		List<Range<Long>> tokenRanges = Lists.newArrayListWithCapacity(tokens
				.size());

		for (Annotation token : tokens) {
			tokenRanges.add(Range.closed(token.getStartNode().getOffset(),
					token.getEndNode().getOffset()));
		}
		return tokenRanges;
	}

	@Override
	public String getOriginalQuery() {
		return query.getOriginalQuery();
	}

	@Override
	public List<QueryTerm> getQueryTerms() {
		return query.getQueryTerms();
	}

	@Override
	public QueryTerm getQueryTermWithID(UUID id) {
		return query.getQueryTermWithID(id);
	}

	@Override
	public List<QueryTerm> getQueryTermsWithType(String... type) {
		return query.getQueryTermsWithType(type);
	}

	@Override
	public List<QueryTerm> getQueryTermsFilteredByPredicate(
			Predicate<QueryTerm> pred) {
		return query.getQueryTermsFilteredByPredicate(pred);
	}

	@Override
	public Query mergeWith(Query... query) {
		return this.query.mergeWith(query);
	}

}
