package de.htwg.iqes.queryexpansion.domain.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;

public final class QueryImpl implements Query {

	private final String originalQuery;

	private final ImmutableList<QueryTerm> queryTerms;

	public QueryImpl(@JsonProperty("originalQuery") String query,
			@JsonProperty("queryTerms") List<QueryTerm> terms) {
		originalQuery = query;
		queryTerms = ImmutableList.copyOf(terms);
	}

	@Override
	public String getOriginalQuery() {
		return originalQuery;
	}

	@Override
	public List<QueryTerm> getQueryTerms() {
		return queryTerms;
	}

	@Override
	public QueryTerm getQueryTermWithID(UUID id) {
		for (QueryTerm term : queryTerms) {
			if (term.getID().equals(id))
				return term;
		}

		return null;
	}

	@Override
	public List<QueryTerm> getQueryTermsWithType(final String... type) {
		Iterable<QueryTerm> filteredTerms = Iterables.filter(queryTerms,
				new Predicate<QueryTerm>() {
					@Override
					public boolean apply(QueryTerm input) {
						List<String> filterTypes = Lists.newArrayList(type);
						return filterTypes.contains(input.getType())
								|| filterTypes.contains(input.getFeatures()
										.get("majorType"));
					}
				});

		return ImmutableList.copyOf(filteredTerms);
	}

	@Override
	public List<QueryTerm> getQueryTermsFilteredByPredicate(
			Predicate<QueryTerm> pred) {
		Iterable<QueryTerm> filteredTerms = Iterables.filter(queryTerms, pred);

		return ImmutableList.copyOf(filteredTerms);
	}

	@Override
	public Query mergeWith(Query... queries) {
		List<Query> queriesToMerge = Lists.newArrayList(queries);
		queriesToMerge.add(this);
		
		Iterable<Query> queriesToMergeFiltered = Iterables.filter(
				queriesToMerge, Predicates.notNull());
		List<QueryTerm> newQueryTerms = Lists.newArrayListWithCapacity(this
				.getQueryTerms().size());

		for (QueryTerm baseTerm : this.getQueryTerms()) {
			Multimap<String, QueryExpansionSet> setsAsMultimap = LinkedHashMultimap
					.create();
			UUID idOfTerm = baseTerm.getID();

			for (Query queryToMerge : queriesToMergeFiltered) {
				QueryTerm term = queryToMerge.getQueryTermWithID(idOfTerm);
				if (term == null)
					continue; // term not in Query

				for (QueryExpansionSet set : term.getExpansionSets()) {
					// Add all sets to the hashed multiset
					setsAsMultimap.put(set.getName(), set);
				}
			}

			// now we've got a multimap with all expansionsets, lets merge them together...
			List<QueryExpansionSet> mergedExpansionSets = Lists.newArrayList();
			for(Map.Entry<String, Collection<QueryExpansionSet>> x : setsAsMultimap.asMap().entrySet()) {
				Multimap<String, String> multimapOfSet = LinkedHashMultimap.create();
				
				for(QueryExpansionSet set : x.getValue()) {
					multimapOfSet.putAll(set.getTermsByFeature());
				}
				
				mergedExpansionSets.add(new QueryExpansionSetImpl(x.getKey(), multimapOfSet));
			}
			
			newQueryTerms.add(new QueryTermImpl(baseTerm.getID(), baseTerm
					.getTerm(), baseTerm.getAlternateForm(),
					baseTerm.getType(), baseTerm.getFeatures(), mergedExpansionSets));
		}

		return new QueryImpl(this.getOriginalQuery(), newQueryTerms);
	}
}
