package de.htwg.iqes.queryexpansion;

import java.util.concurrent.Callable;

import com.google.common.base.Preconditions;

import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.queryexpansion.domain.Query;

/**
 * Abstraction of a expansion job.
 * 
 * @author "Julian Müller"
 * 
 */
public class ExpandQueryWithQE implements Callable<Query> {

	private final QueryExpansion qe;

	private final Query query;

	public ExpandQueryWithQE(QueryExpansion qe, Configuration config,
			Query query) {
		Preconditions.checkNotNull(qe,
				"QueryExpansion Module must not be null.");
		Preconditions.checkNotNull(config, "Configuration must not be null.");
		Preconditions.checkNotNull(query, "Query must not be null.");

		this.qe = qe;
		this.query = query;
		this.qe.setConfiguration(config);
	}

	@Override
	public Query call() throws Exception {
		return qe.expandQuery(query);
	}

}
