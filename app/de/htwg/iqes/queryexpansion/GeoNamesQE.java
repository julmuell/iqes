package de.htwg.iqes.queryexpansion;

import static akka.dispatch.Futures.future;
import static akka.dispatch.Futures.sequence;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.geonames.FeatureClass;
import org.geonames.Style;
import org.geonames.Toponym;
import org.geonames.ToponymSearchCriteria;
import org.geonames.ToponymSearchResult;
import org.geonames.WebService;

import play.libs.Akka;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorSystem;
import akka.util.Timeout;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;

import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;
import de.htwg.iqes.queryexpansion.domain.impl.QueryExpansionSetImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryTermImpl;

/**
 * The Query Expansion module for Expansion By GeoNames. The module is using the
 * REST api of GeoNames ({@link api.geonames.org}).<br>
 * <br>
 * FeatureClasses describe the entities (categorization schema): <br>
 * <ul>
 * <li>FeatureClass.A - country, state, region,...</li>
 * <li>FeatureClass.H - stream, lake,...</li>
 * <li>FeatureClass.L - parks,area,...</li>
 * <li>FeatureClass.P - city, village,...</li>
 * <li>FeatureClass.R - road, railroad</li>
 * <li>FeatureClass.S - spot, building, farm</li>
 * <li>FeatureClass.T - mountain,hill,rock,...</li>
 * <li>FeatureClass.U - undersea</li>
 * <li>FeatureClass.V - forest,heath,...</li>
 * </ul>
 * 
 * @author "Julian Müller"
 * 
 */
public class GeoNamesQE extends AbstractQueryExpansion {

	private static final String MODULE = "GeoNamesQE";

	private static final int INITIAL_SEARCH_MAX_ROWS = 50;

	private static final String USERNAME = "julmuell";

	static Logger logger = Logger.getLogger(GeoNamesQE.class);

	@Override
	protected Query expandQueryConditional(Query query) {
		Preconditions.checkNotNull(configuration,
				"Configuration must not be null.");
		Preconditions.checkNotNull(query, "Query must not be null.");

		List<Future<QueryTerm>> futures = Lists.newArrayList();
		ActorSystem system = Akka.system();

		for (final QueryTerm term : query.getQueryTermsWithType("Town",
				"Settlement", "City", "Continent", "AdministrativeRegion",
				"Mountain", "Location", "Country")) {
			futures.add(future(new Callable<QueryTerm>() {
				@Override
				public QueryTerm call() throws Exception {
					logger.info("Expand term " + term.getTerm());

					QueryExpansionSet expansionSet = expandQuery(term.getTerm());

					logger.info("Found "
							+ expansionSet.getTermsByFeature().size()
							+ " Expansions for " + term.getTerm());

					return new QueryTermImpl(term.getID(), term.getTerm(), term
							.getAlternateForm(), term.getType(), term
							.getFeatures(), Lists.newArrayList(expansionSet));

				}
			}, system.dispatcher()));
		}

		Future<Iterable<QueryTerm>> futuereListOfQueryTerms = sequence(futures,
				system.dispatcher());

		Timeout timeout = new Timeout(Duration.create(5, "seconds"));

		Iterable<QueryTerm> results = Lists.newArrayList();

		try {
			results = Await.result(futuereListOfQueryTerms, timeout.duration());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new QueryImpl(query.getOriginalQuery(),
				Lists.newArrayList(results));
	}

	private QueryExpansionSet expandQuery(String query) {
		Preconditions.checkNotNull(configuration,
				"Configuration must not be null.");

		final Set<FeatureClass> priorities = getMergedPrioritySet(configuration
				.getPriorityOrderForGeoNamesMatching());
		logger.debug(priorities);
		logger.debug(configuration.getPriorityOrderForGeoNamesMatching());
		WebService.setUserName(USERNAME);

		ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
		searchCriteria.setName(query);
		searchCriteria.setMaxRows(INITIAL_SEARCH_MAX_ROWS);

		ToponymSearchResult result = null;

		try {
			result = WebService.search(searchCriteria);
		} catch (Exception e) {
			logger.error("Can't find a suitable toponym with fulltext search",
					e);
			return new QueryExpansionSetImpl(MODULE,
					LinkedListMultimap.<String, String> create());
		}

		Toponym firstToponymResult = null;

		try {
			firstToponymResult = findFirstToponymWithFeatureclass(priorities,
					result.getToponyms());
		} catch (NoSuchElementException e) {
			logger.error("No Toponym found", e);
			return new QueryExpansionSetImpl(MODULE,
					LinkedListMultimap.<String, String> create());
		}

		return expandToponym(firstToponymResult);
	}

	private QueryExpansionSet expandToponym(Toponym firstToponymResult) {
		logger.debug("Expand Toponym " + firstToponymResult.getName()
				+ " with FeatureClass "
				+ firstToponymResult.getFeatureClassName());

		List<Toponym> expansionTerms = null;
		String expansionType = null;

		switch (firstToponymResult.getFeatureClass()) {
		case H:
		case P:
		case S:
		case T:
		case U:
		case V:
			expansionTerms = findCitiesWithinRadius(
					firstToponymResult.getLatitude(),
					firstToponymResult.getLongitude(),
					configuration.getExpansionRadiusForGeoNames(),
					configuration.getMaxExpansionRowsForGeoNames());
			expansionType = "citiesWithinRadius";
			break;
		case A:
		case L:
			expansionTerms = findChildrenWithinBoundaryBox(firstToponymResult
					.getGeoNameId());
			expansionType = "childrenWithinBoundaryBox";
			break;
		default:
		}

		if (expansionTerms != null) {
			logger.debug("Expansion with " + expansionType + " feature.");
			List<String> expansionTermsAsString = transformToponymToString(expansionTerms);

			Multimap<String, String> mm = LinkedListMultimap.create();
			mm.putAll(expansionType, expansionTermsAsString);

			return new QueryExpansionSetImpl(MODULE, mm);
		} else {
			return new QueryExpansionSetImpl(MODULE,
					LinkedListMultimap.<String, String> create());
		}
	}

	/**
	 * The method tries to find the first result in {@code results} according to
	 * the priority list.<br>
	 * <br>
	 * <b>For example</b>: priority list: [A,P] <br>
	 * results: [P,P,P,A] => result: A.<br>
	 * results: [P,P,P] => result: P.
	 * 
	 * @param priorities
	 * @param results
	 * @return
	 */
	private Toponym findFirstToponymWithFeatureclass(
			final Set<FeatureClass> priorities, List<Toponym> results) {
		Ordering<Toponym> sortByPriority = Ordering.explicit(
				Lists.newLinkedList(priorities)).onResultOf(
				new Function<Toponym, FeatureClass>() {
					@Override
					public FeatureClass apply(Toponym input) {
						return input.getFeatureClass();
					}
				});

		return Iterables.find(sortByPriority.immutableSortedCopy(results),
				new Predicate<Toponym>() {
					@Override
					public boolean apply(Toponym input) {
						// The Collection is already sorted according to the
						// priority list. So we need only to check if the
						// FeatureClass of the item is within the priority list.
						return priorities.contains(input.getFeatureClass());
					}
				});
	}

	private List<Toponym> findCitiesWithinRadius(double latitude,
			double longitude, double radius, int maxRows) {
		// We only want to expand with Toponym's with the following class.
		FeatureClass FILTER_ON = FeatureClass.P;

		List<Toponym> cities = null;
		String featureCodes[] = null;
		String language = "de";

		try {
			cities = WebService.findNearby(latitude, longitude, radius,
					FILTER_ON, featureCodes, language, maxRows);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (cities == null) ? new LinkedList<Toponym>() : cities;
	}

	private List<Toponym> findChildrenWithinBoundaryBox(int geoNamesID) {
		String language = "de";
		List<Toponym> cities = null;

		try {
			cities = WebService.children(geoNamesID, language, Style.SHORT)
					.getToponyms();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (cities == null) ? Lists.<Toponym> newArrayList() : cities;
	}

	/**
	 * Merges two Sets into one. First the {@code priority} and than the entire
	 * EnumSet of FeatureClass.
	 * 
	 * @param priority
	 * @return
	 */
	private Set<FeatureClass> getMergedPrioritySet(Set<FeatureClass> priority) {
		return ImmutableSet.<FeatureClass> builder().addAll(priority)
				.addAll(Arrays.asList(FeatureClass.values())).build();
	}

	/**
	 * We only want to deliver String instead of the Type Toponym, so we
	 * transform the Toponym to String. Due to transformation we loose the
	 * entire Toponym informations.
	 * 
	 * @param expansionTerms
	 * @return
	 */
	private List<String> transformToponymToString(List<Toponym> expansionTerms) {
		List<String> expansionTermsAsString = Lists.transform(expansionTerms,
				new Function<Toponym, String>() {
					@Override
					public String apply(Toponym input) {
						return input.getName();
					}
				});
		return expansionTermsAsString;
	}

	@Override
	protected boolean isExpansionActive() {
		return configuration.isExpansionByGeoNamesActive();
	}
	
	@Override
	public String getModuleName() {
		return MODULE;
	}
}
