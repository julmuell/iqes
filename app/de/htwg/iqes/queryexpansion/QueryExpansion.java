package de.htwg.iqes.queryexpansion;

import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.queryexpansion.domain.Query;

public interface QueryExpansion {
	Query expandQuery(Query query);

	void setConfiguration(Configuration config);
	
	String getModuleName();
}
