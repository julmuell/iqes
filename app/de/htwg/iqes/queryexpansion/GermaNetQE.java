package de.htwg.iqes.queryexpansion;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;
import de.htwg.iqes.queryexpansion.domain.impl.QueryExpansionSetImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryTermImpl;
import de.tuebingen.uni.sfs.germanet.api.ConRel;
import de.tuebingen.uni.sfs.germanet.api.GermaNet;
import de.tuebingen.uni.sfs.germanet.api.LexUnit;
import de.tuebingen.uni.sfs.germanet.api.Synset;

/**
 * The Query Expansion module for expansion by GermaNet. 
 * 
 * @author "Julian Müller"
 *
 */
public class GermaNetQE extends AbstractQueryExpansion {

	private static final String MODULE = "GermaNetQE";

	private static final List<POSGermanTag> validCategories = Lists
			.newArrayList(POSGermanTag.ADJA, POSGermanTag.ADJD,
					POSGermanTag.NE, POSGermanTag.NN, POSGermanTag.VVFIN,
					POSGermanTag.VVIMP, POSGermanTag.VVINF, POSGermanTag.VVIZU,
					POSGermanTag.VVPP);

	@Inject
	private GermaNet germanet;

	@Override
	protected Query expandQueryConditional(Query query) {
		Preconditions.checkNotNull(configuration,
				"Configuration must not be null.");
		Preconditions.checkNotNull(query, "Query must not be null.");

		List<QueryTerm> queryTerms = Lists.newArrayList();

		Predicate<QueryTerm> filterByValidCategories = new Predicate<QueryTerm>() {
			@Override
			public boolean apply(QueryTerm term) {
				Optional<String> category = Optional.fromNullable(term
						.getFeatures().get("category"));

				if (!category.isPresent())
					return false;

				for (POSGermanTag t : POSGermanTag.values()) {
					// avoids IllegalArgumentException due to incompatible type.
					if (t.toString().equalsIgnoreCase(category.get()))
						return validCategories.contains(POSGermanTag
								.valueOf(category.get()));
				}

				return false;
			}
		};

		for (QueryTerm term : query
				.getQueryTermsFilteredByPredicate(filterByValidCategories)) {
			queryTerms.add(new QueryTermImpl(term.getID(), term.getTerm(), term
					.getAlternateForm(), term.getType(), term.getFeatures(),
					Lists.newArrayList(expandQuery(term.getTerm(),
							term.getAlternateForm()))));
		}

		return new QueryImpl(query.getOriginalQuery(), queryTerms);
	}

	private QueryExpansionSet expandQuery(String term, String alternateTerm) {
		List<Synset> synsets = germanet.getSynsets(term);

		if (synsets.size() == 0)
			// Second try.
			synsets = germanet.getSynsets(alternateTerm);

		if (synsets.size() == 0) {
			// return empty QueryExpansionSet.
			return new QueryExpansionSetImpl(MODULE,
					LinkedListMultimap.<String, String> create());
		} else {
			Multimap<String, String> map = LinkedListMultimap.create();

			for (Synset s : synsets) {
				map.putAll("synonyms", s.getAllOrthForms());
				map.putAll("hyponym",
						getLexUnitsOfRelatedSynsets(s, ConRel.has_hyponym));
			}

			return new QueryExpansionSetImpl(MODULE, map);
		}
	}

	private List<String> getLexUnitsOfRelatedSynsets(Synset synset, ConRel rel) {
		List<String> lexUnits = new LinkedList<String>();

		for (Synset s : synset.getRelatedSynsets(rel)) {
			List<LexUnit> foundLexUnits = s.getLexUnits();

			for (LexUnit l : foundLexUnits) {
				lexUnits.add(l.getOrthForm());
			}
		}

		return lexUnits;
	}

	@Override
	protected boolean isExpansionActive() {
		return configuration.isExpansionByGermaNetActive();
	}
	
	@Override
	public String getModuleName() {
		return MODULE;
	}
}
