package de.htwg.iqes.queryexpansion.dbpedia;

import static akka.dispatch.Futures.future;
import static akka.dispatch.Futures.sequence;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import play.libs.Akka;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorSystem;
import akka.util.Timeout;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import de.htwg.iqes.global.di.modules.GuiceModule.DBpediaQueryExpansion;
import de.htwg.iqes.queryexpansion.AbstractQueryExpansion;
import de.htwg.iqes.queryexpansion.ExpandQueryWithQE;
import de.htwg.iqes.queryexpansion.QueryExpansion;
import de.htwg.iqes.queryexpansion.domain.Query;

/**
 * The Query Expansion module for Expansion By DBpedia. 
 * 
 * @author "Julian Müller"
 *
 */
public class DBpediaQE extends AbstractQueryExpansion {

	public static final String MODULE = "DBpediaQE";

	static Logger logger = Logger.getLogger(DBpediaQE.class);
	
	@Inject
	@DBpediaQueryExpansion
	public Set<QueryExpansion> dbpediaQueryExpansionModules;

	@Override
	protected Query expandQueryConditional(Query query) {
		Preconditions.checkNotNull(configuration,
				"Configuration must not be null.");
		Preconditions.checkNotNull(query, "Query must not be null.");
		
		logger.info("Start expansion of Query '" + query.getOriginalQuery()
				+ "'");
		
		ActorSystem system = Akka.system();

		Stopwatch watch = Stopwatch.createStarted();
		List<Future<Query>> futures = Lists.newArrayList();

		for (final QueryExpansion queryExpansionModule : dbpediaQueryExpansionModules) {
			futures.add(future(new ExpandQueryWithQE(queryExpansionModule,
					configuration, query), system.dispatcher()));
		}

		Future<Iterable<Query>> futuereListOfQueries = sequence(futures,
				system.dispatcher());

		Timeout timeout = new Timeout(Duration.create(10,
				"seconds"));

		Iterable<Query> results = null;

		try {
			// Wait on futures.
			results = Await.result(futuereListOfQueries, timeout.duration());
		} catch (Exception e) {
			e.printStackTrace();
			results = Lists.newArrayList(); // otherwise causing Null Pointer.
		}

		logger.info("Time taken for expansion: "
				+ watch.elapsed(TimeUnit.MILLISECONDS));

		return query.mergeWith(Iterables.toArray(results, Query.class));
	}
	
	@Override
	protected boolean isExpansionActive() {
		return configuration.isExpansionByLODActive();
	}
	
	@Override
	public String getModuleName() {
		return MODULE;
	}

}
