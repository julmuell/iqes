package de.htwg.iqes.queryexpansion.dbpedia;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;

public class DBpediaUtil {
	public static ResultSet executeSparqlQuery(String q) {
		// We're using the extensions from Virtuoso on the dbpedia store BUT
		// Jena isn't able to parse such queries. Thats why we can't parse the
		// query, instead we have to send it directly.
		QueryExecution x = new QueryEngineHTTP("http://dbpedia.org/sparql", q);

		ResultSet results = x.execSelect();

		try {
			results = x.execSelect();
		} catch (QueryExceptionHTTP err) {
			err.printStackTrace();
		}

		return results;
	}

	public static String getSparqlQuery(String path) {
		StringWriter writer = new StringWriter();

		try {
			InputStream in = new FileInputStream(new File(path));
			IOUtils.copy(in, writer, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return writer.toString();
	}
}
