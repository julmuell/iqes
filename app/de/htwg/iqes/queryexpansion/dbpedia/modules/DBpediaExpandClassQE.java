package de.htwg.iqes.queryexpansion.dbpedia.modules;

import java.util.List;
import java.util.Set;

import com.google.common.base.Predicate;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;

import de.htwg.iqes.queryexpansion.AbstractQueryExpansion;
import de.htwg.iqes.queryexpansion.dbpedia.DBpediaQE;
import de.htwg.iqes.queryexpansion.dbpedia.DBpediaUtil;
import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;
import de.htwg.iqes.queryexpansion.domain.impl.QueryExpansionSetImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryTermImpl;

public class DBpediaExpandClassQE extends AbstractQueryExpansion {

	private static String MODULE = "DBpediaQE-DBpediaExpandClassQE";

	@Override
	protected Query expandQueryConditional(Query q) {
		String sparqlQuery = DBpediaUtil
				.getSparqlQuery("./conf/sparql/classInstancesSparqlQuery.txt");
		List<QueryTerm> newQueryTerms = Lists.newArrayList();

		for (QueryTerm term : q
				.getQueryTermsFilteredByPredicate(new Predicate<QueryTerm>() {

					@Override
					public boolean apply(QueryTerm term) {
						return term.getFeatures().containsKey("type")
								&& term.getFeatures().get("type")
										.equals("class");
					}

				})) {

			Multimap<String, String> map = LinkedHashMultimap.create();
			Set<String> expansionLabels = Sets.newLinkedHashSet();

			String dbPediaClass = term.getFeatures().get("URI");

			ResultSet results = DBpediaUtil.executeSparqlQuery(String.format(
					sparqlQuery, dbPediaClass));

			while (results.hasNext()) {
				QuerySolution solution = results.nextSolution();
				RDFNode label = solution.get("label");

				if (label != null && label.isLiteral())
					expansionLabels.add(((Literal) label).getLexicalForm());
			}

			map.putAll("instancesOfClass", expansionLabels);

			QueryExpansionSet set = new QueryExpansionSetImpl(DBpediaQE.MODULE,
					map);

			newQueryTerms.add(new QueryTermImpl(term.getID(), term.getTerm(),
					term.getAlternateForm(), term.getType(),
					term.getFeatures(), Lists.newArrayList(set)));
		}

		return new QueryImpl(q.getOriginalQuery(), newQueryTerms);
	}

	@Override
	protected boolean isExpansionActive() {
		return true;
	}

	@Override
	public String getModuleName() {
		return MODULE;
	}
}
