package de.htwg.iqes.queryexpansion.dbpedia.modules;

import java.util.List;
import java.util.Set;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

import de.htwg.iqes.queryexpansion.AbstractQueryExpansion;
import de.htwg.iqes.queryexpansion.dbpedia.DBpediaQE;
import de.htwg.iqes.queryexpansion.dbpedia.DBpediaUtil;
import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.QueryExpansionSet;
import de.htwg.iqes.queryexpansion.domain.QueryTerm;
import de.htwg.iqes.queryexpansion.domain.impl.QueryExpansionSetImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryImpl;
import de.htwg.iqes.queryexpansion.domain.impl.QueryTermImpl;

public class DBpediaExpandMusicGenresQE extends AbstractQueryExpansion {

	private static String MODULE = "DBpediaQE-DBpediaExpandMusicGenresQE";

	@Override
	protected Query expandQueryConditional(Query q) {
		String sparqlQuery = DBpediaUtil
				.getSparqlQuery("./conf/sparql/musicSparqlQuery.txt");
		List<QueryTerm> newQueryTerms = Lists.newArrayList();

		for (QueryTerm term : q.getQueryTermsWithType("MusicGenre")) {
			Multimap<String, String> map = LinkedHashMultimap.create();
			Set<String> expansionLabels = Sets.newLinkedHashSet();

			ResultSet results = DBpediaUtil.executeSparqlQuery(String.format(
					sparqlQuery, term.getFeatures().get("inst")));

			while (results.hasNext()) {
				QuerySolution solution = results.nextSolution();
				RDFNode label = solution.get("label");

				if (label != null)
					expansionLabels.add((label.toString().split("@"))[0]);
			}

			map.putAll("musicArtists", expansionLabels);

			QueryExpansionSet set = new QueryExpansionSetImpl(DBpediaQE.MODULE,
					map);

			newQueryTerms.add(new QueryTermImpl(term.getID(), term.getTerm(),
					term.getAlternateForm(), term.getType(),
					term.getFeatures(), Lists.newArrayList(set)));
		}

		return new QueryImpl(q.getOriginalQuery(), newQueryTerms);
	}

	@Override
	protected boolean isExpansionActive() {
		return true;
	}

	@Override
	public String getModuleName() {
		return MODULE;
	}

}
