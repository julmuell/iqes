package de.htwg.iqes.queryexpansion;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;

import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.queryexpansion.domain.Query;

/**
 * The AbstractQueryExpansion class has got the main execution methodes of the
 * expansionsystem. A new query expansion module has to implement the following
 * class.
 * 
 * @author "Julian Müller"
 * 
 */
public abstract class AbstractQueryExpansion implements QueryExpansion {

	Logger logger = Logger.getLogger(AbstractQueryExpansion.class);

	protected Configuration configuration;

	@Override
	public void setConfiguration(Configuration config) {
		Preconditions.checkNotNull(config, "Configuration must not be null");

		this.configuration = config;
	}

	/**
	 * Gets executed by other modules.
	 */
	@Override
	public Query expandQuery(Query query) {
		Query queryToReturn = query;

		logger.info("Expanding by Module: " + this.getModuleName());
		Stopwatch watch = Stopwatch.createStarted();

		if (isExpansionActive())
			queryToReturn = expandQueryConditional(query);

		logger.info("Expansion by Module: " + this.getModuleName()
				+ " has taken " + watch.elapsed(TimeUnit.MILLISECONDS) + " ms");

		return queryToReturn;
	}

	protected abstract Query expandQueryConditional(Query query);

	protected abstract boolean isExpansionActive();
}
