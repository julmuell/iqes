package de.htwg.iqes.controller;

import static akka.dispatch.Futures.future;
import static akka.dispatch.Futures.sequence;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.util.InvalidOffsetException;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import play.api.Play;
import play.libs.Akka;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorSystem;
import akka.util.Timeout;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Optional;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import de.htwg.iqes.domain.QueryForm;
import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.domain.configuration.ConfigurationFactory;
import de.htwg.iqes.preprocessing.Preprocessor;
import de.htwg.iqes.queryexpansion.ExpandQueryWithQE;
import de.htwg.iqes.queryexpansion.QueryExpansion;
import de.htwg.iqes.queryexpansion.domain.Query;

/**
 * Main entrance point for using the Intelligent Query Expansion System.
 * 
 * @author "Julian Müller"
 * 
 */
public class IQESController {

	private static final String EXECUTION_TIME_IN = "seconds";

	private static final int DEFAULT_MAX_EXECUTION_TIME = 10;

	@Inject
	private Set<QueryExpansion> queryExpansionModules;

	@Inject
	private ConfigurationFactory configFactory;

	@Inject
	private Preprocessor preprocessor;

	/*
	 * We're using the actorsystem of the Play! environment.
	 */
	private ActorSystem system = Akka.system();

	static Logger logger = Logger.getLogger(IQESController.class);

	/**
	 * Expand a query with a String and optionally with a configuration. The
	 * configuration has to be encoded as a valid JSON Document.
	 * 
	 * @param query
	 * @param configuration
	 * @return merged Query
	 */
	public Query expandQuery(String query, Optional<String> configuration) {
		Configuration config = getOrCreateConfiguration(configuration);

		return expandQuery(preprocess(query, config), config);
	}

	/**
	 * Expand a query with a String and a queryForm.
	 * 
	 * @param query
	 * @param configuration
	 * @return merged Query
	 */
	public Query expandQuery(String query, QueryForm configuration) {
		Configuration config = configFactory.createWithQueryForm(configuration);

		return expandQuery(preprocess(query, config), config);
	}

	/**
	 * Expand a query with a String and a already configured configuration.
	 * 
	 * @param query
	 * @param config
	 * @return merged query
	 */
	public Query expandQuery(String query, Configuration config) {
		return expandQuery(preprocess(query, config), config);
	}

	/**
	 * Expand a query with a already configured string and configuration.
	 * 
	 * The following methode is the main expansion methode of IQES. The
	 * multithreaded processing of a query gets executed within the following
	 * methode.
	 * 
	 * @param query
	 * @param configuration
	 * @return merged query
	 */
	public Query expandQuery(final Query query, Configuration configuration) {
		logger.info("Start expansion of query '" + query.getOriginalQuery()
				+ "'");

		Stopwatch watch = Stopwatch.createStarted();
		List<Future<Query>> futures = Lists.newArrayList();

		for (final QueryExpansion queryExpansionModule : queryExpansionModules) {
			futures.add(future(new ExpandQueryWithQE(queryExpansionModule,
					configuration, query), system.dispatcher()));
		}

		Future<Iterable<Query>> futuereListOfQueries = sequence(futures,
				system.dispatcher());

		play.Configuration playConf = new play.Configuration(Play.current()
				.configuration());
		Integer maxExecutionTime = playConf.getInt("maxExecutionTimeInSeconds");

		Timeout timeout = new Timeout(Duration.create(
				(maxExecutionTime != null) ? maxExecutionTime
						: DEFAULT_MAX_EXECUTION_TIME, EXECUTION_TIME_IN));

		Iterable<Query> results = null;

		try {
			// Wait on futures.
			results = Await.result(futuereListOfQueries, timeout.duration());
		} catch (Exception e) {
			e.printStackTrace();
			results = Lists.newArrayList(); // otherwise causing Null Pointer.
		}

		logger.info("Time taken for expansion: "
				+ watch.elapsed(TimeUnit.MILLISECONDS));

		return postprocess(query, results);
	}

	/**
	 * Helper methode to provide a valid configuration.
	 * 
	 * @param configuration
	 * @return valid configuration
	 */
	private Configuration getOrCreateConfiguration(
			Optional<String> configuration) {
		Configuration conf;

		if (configuration.isPresent()) {
			conf = configFactory.createWithJson(configuration);
		} else {
			conf = configFactory.get();
		}

		return conf;
	}

	/**
	 * Executes the preprocessing layer.
	 * 
	 * @param query
	 * @return preprocessed query
	 */
	public Query preprocess(String query) {
		return preprocess(query, configFactory.get());
	}

	/**
	 * Executes the preprocessing layer with a configuration.
	 * 
	 * @param query
	 * @param configuration
	 * @return preprocessd query
	 */
	public Query preprocess(String query, Optional<String> configuration) {
		Configuration config = getOrCreateConfiguration(configuration);

		return preprocess(query, config);
	}

	/**
	 * Executes the preprocessing layer with an optional configuration.
	 * 
	 * @param query
	 * @param configuration
	 * @return preprocessd query
	 */
	public Query preprocess(String query, Configuration configuration) {
		preprocessor.setConfiguration(configuration);

		try {
			return preprocessor.preprocess(query);
		} catch (InvalidOffsetException | ResourceInstantiationException
				| ExecutionException | JsonProcessingException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Merging the queries.
	 * 
	 * @param query
	 * @param queries
	 * @return merged queries
	 */
	public Query postprocess(Query query, Iterable<Query> queries) {
		return query.mergeWith(Iterables.toArray(queries, Query.class));
	}

}
