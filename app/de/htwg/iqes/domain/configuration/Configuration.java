package de.htwg.iqes.domain.configuration;

import java.util.Map;
import java.util.Set;

import org.geonames.FeatureClass;

/**
 * The possible configurations within IQES.
 * 
 * @author "Julian Müller"
 *
 */
public interface Configuration {
	boolean isExpansionByGeoNamesActive() ;

	boolean isExpansionByGermaNetActive();

	boolean isExpansionByLODActive();

	int getExpansionRadiusForGeoNames();
	
	int getMaxExpansionRowsForGeoNames();

	Set<FeatureClass> getPriorityOrderForGeoNamesMatching();
	
	Map<String,Boolean> getWhichPRShouldrun();
}
