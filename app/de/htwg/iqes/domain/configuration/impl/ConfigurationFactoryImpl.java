package de.htwg.iqes.domain.configuration.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Set;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.geonames.FeatureClass;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.google.common.collect.Sets;

import de.htwg.iqes.domain.QueryForm;
import de.htwg.iqes.domain.QueryForm.QueryConfigurationForm;
import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.domain.configuration.ConfigurationFactory;

/**
 * The Factory to get or create Configuration Objects.
 * 
 * @author "Julian Müller"
 * 
 */
public class ConfigurationFactoryImpl implements ConfigurationFactory {

	private static final Class<ConfigurationImpl> CLASSTORETURN = ConfigurationImpl.class;

	private final Configuration defaultConfig;

	private final ObjectMapper mapper;

	private final Logger log;

	/**
	 * Gets called by the injection.
	 * 
	 * @param config
	 * @param mapper
	 * @param log
	 */
	@Inject
	public ConfigurationFactoryImpl(Configuration config, ObjectMapper mapper,
			Logger log) {
		defaultConfig = checkNotNull(config, "Default conig must not be null");
		this.mapper = checkNotNull(mapper, "Object Mapper must not be null");
		this.log = checkNotNull(log, "Logger must not be null");
	}

	/**
	 * @return The default Configuration.
	 */
	@Override
	public Configuration get() {
		return defaultConfig;
	}

	/**
	 * Tries to create a IQES-Configuration with the parameter
	 * <i>deserializableConfig</i>. For the mapping from Json to Configuration
	 * the methode uses the ObjectMapper ({@link ObjectMapper})
	 * 
	 * @return Configuration, if the deserialization wasn't successfull the
	 *         default config 'll be returned!
	 */
	@Override
	public Configuration createWithJson(Optional<String> deserializableConfig) {
		checkNotNull(deserializableConfig, "Json String must not be null");

		Configuration configToReturn = null;

		if (!deserializableConfig.isPresent()) {
			return defaultConfig;
		}

		try {
			configToReturn = mapper.readValue(deserializableConfig.get(),
					CLASSTORETURN);
		} catch (IOException e) {
			// TODO silently ignore?
			log.warning(e.getMessage());
		}

		return Optional.fromNullable(configToReturn).or(defaultConfig);
	}

	/**
	 * Tries to create a IQES-Configuration with the Play! form of a queryform.
	 * 
	 * @return Configuration, if the deserialization wasn't successfull the
	 *         default config 'll be returned!
	 */
	@Override
	public Configuration createWithQueryForm(QueryForm query) {
		checkNotNull(query, "Query form must not be null");
		QueryConfigurationForm queryConfig = query.configuration;

		Set<FeatureClass> set = Sets.newLinkedHashSet();

		for (FeatureClass cls : queryConfig.priorityOrderForGeoNamesMatching) {
			if (cls != null)
				set.add(cls);
		}

		return new ConfigurationImpl(queryConfig.isExpansionByGeoNamesActive,
				queryConfig.isExpansionByGermaNetActive,
				queryConfig.isExpansionByLODActive,
				queryConfig.expansionRadiusForGeoNames,
				queryConfig.maxExpansionRowsForGeoNames, set,
				queryConfig.whichPRShouldrun);
	}
}
