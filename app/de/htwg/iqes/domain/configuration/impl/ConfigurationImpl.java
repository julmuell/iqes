package de.htwg.iqes.domain.configuration.impl;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.geonames.FeatureClass;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.htwg.iqes.domain.configuration.Configuration;

/**
 * Holds the entire Configuration of the preprocessing layer.
 * 
 * @author "Julian Müller"
 * 
 */
public /*final*/ class ConfigurationImpl implements Configuration {
	/*
	 * The Expansion Modules are completely separated, so it's possible to
	 * deactivate the Expansion by specific Modules.
	 */
	private final boolean isExpansionByGeoNamesActive;
	private final boolean isExpansionByGermaNetActive;
	private final boolean isExpansionByLODActive;

	/*
	 * Configuration specific to the GeoNames Module.
	 */
	private final int expansionRadiusForGeoNames;
	private final int maxExpansionRowsForGeoNames;
	private final Set<FeatureClass> priorityOrderForGeoNamesMatching;

	/*
	 * Configuration specific to the Preprocessing phase.
	 */
	private final Map<String, Boolean> whichPRShouldrun;

	@JsonCreator
	public ConfigurationImpl(
			@JsonProperty("isExpansionByGeoNamesActive") boolean isExpansionByGeoNamesActive,
			@JsonProperty("isExpansionByGermaNetActive") boolean isExpansionByGermaNetActive,
			@JsonProperty("isExpansionByLODActive") boolean isExpansionByLODActive,
			@JsonProperty("expansionRadiusForGeoNames") int expansionRadius,
			@JsonProperty("maxExpansionRowsForGeoNames") int maxExpansionRowsForGeoNames,
			@JsonProperty("priorityOrderForGeoNamesMatching") Set<FeatureClass> priorityOrderForMatching,
			@JsonProperty("whichPRShouldrun") Map<String, Boolean> whichPRShouldrun) {
		this.isExpansionByGeoNamesActive = isExpansionByGeoNamesActive;
		this.isExpansionByGermaNetActive = isExpansionByGermaNetActive;
		this.isExpansionByLODActive = isExpansionByLODActive;
		this.expansionRadiusForGeoNames = expansionRadius;
		this.maxExpansionRowsForGeoNames = maxExpansionRowsForGeoNames;
		this.priorityOrderForGeoNamesMatching = priorityOrderForMatching;
		this.whichPRShouldrun = whichPRShouldrun;
	}

	public boolean isExpansionByGeoNamesActive() {
		return isExpansionByGeoNamesActive;
	}

	public boolean isExpansionByGermaNetActive() {
		return isExpansionByGermaNetActive;
	}

	public boolean isExpansionByLODActive() {
		return isExpansionByLODActive;
	}

	public int getExpansionRadiusForGeoNames() {
		return expansionRadiusForGeoNames;
	}

	public int getMaxExpansionRowsForGeoNames() {
		return maxExpansionRowsForGeoNames;
	}

	public Set<FeatureClass> getPriorityOrderForGeoNamesMatching() {
		return priorityOrderForGeoNamesMatching;
	}

	public Map<String, Boolean> getWhichPRShouldrun() {
		return whichPRShouldrun;
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;

		if (this.getClass() != obj.getClass())
			return false;

		final ConfigurationImpl other = (ConfigurationImpl) obj;

		return Objects.equals(this.expansionRadiusForGeoNames, other.expansionRadiusForGeoNames)
				&& Objects.equals(this.isExpansionByGeoNamesActive,
						other.isExpansionByGeoNamesActive)
				&& Objects.equals(this.isExpansionByGermaNetActive,
						other.isExpansionByGermaNetActive)
				&& Objects.equals(this.isExpansionByLODActive, other.isExpansionByLODActive)
				&& Objects.equals(this.priorityOrderForGeoNamesMatching,
						other.priorityOrderForGeoNamesMatching);
	}

	public String toString() {
		return com.google.common.base.Objects.toStringHelper(this)
				.add("Expansion By GeoNames?", isExpansionByGeoNamesActive)
				.add("Expansion By GermaNet?", isExpansionByGermaNetActive)
				.add("Expansion By LOD?", isExpansionByLODActive)
				.add("Expansion Radius for GeoNames", expansionRadiusForGeoNames)
				.add("Priority Order for GeoNames?", priorityOrderForGeoNamesMatching).toString();
	}

}
