package de.htwg.iqes.domain.configuration;

import com.google.common.base.Optional;

import de.htwg.iqes.domain.QueryForm;

/**
 * The following class hat the purpose to abstract the creation of configuration
 * objects from different types.
 * 
 * @author "Julian Müller"
 * 
 */
public interface ConfigurationFactory {
	public Configuration get();

	public Configuration createWithJson(Optional<String> deserializableConfig);

	public Configuration createWithQueryForm(QueryForm query);
}
