package de.htwg.iqes.domain;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.geonames.FeatureClass;

import play.data.validation.Constraints.Max;
import play.data.validation.Constraints.Required;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * The Play! Form of a query. The main purpose of these class is to visualize
 * the different parameters of the IQES system.
 * 
 * @author "Julian Müller"
 * 
 */
public class QueryForm {
	@Required
	public String query;

	@Valid
	public QueryConfigurationForm configuration;

	public QueryForm() {
		configuration = new QueryConfigurationForm();
	}

	public static class QueryConfigurationForm {
		@Required
		public Boolean isExpansionByGeoNamesActive;

		@Required
		public Boolean isExpansionByGermaNetActive;

		@Required
		public Boolean isExpansionByLODActive;

		@Required
		@Max(20)
		public Integer expansionRadiusForGeoNames;

		@Required
		@Max(100)
		public Integer maxExpansionRowsForGeoNames;

		@Valid
		public List<FeatureClass> priorityOrderForGeoNamesMatching;

		public Map<String, Boolean> whichPRShouldrun;

		public QueryConfigurationForm() {
			isExpansionByGeoNamesActive = isExpansionByGermaNetActive = isExpansionByLODActive = true;
			expansionRadiusForGeoNames = 5;
			maxExpansionRowsForGeoNames = 50;
			priorityOrderForGeoNamesMatching = Lists.newArrayList();
			whichPRShouldrun = Maps.newLinkedHashMap();
		}
	}

	public static Map<String, String> getFC() {
		Map<String, String> fc = Maps.newHashMap();

		for (FeatureClass cls : FeatureClass.values()) {
			fc.put(cls.name(), cls.name());
		}

		return fc;
	}
}
