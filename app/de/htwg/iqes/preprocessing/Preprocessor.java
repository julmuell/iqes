package de.htwg.iqes.preprocessing;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.queryexpansion.domain.Query;
import de.htwg.iqes.queryexpansion.domain.impl.GATEDocumentAdapter;
import easypool.ObjectPool;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.creole.AnalyserRunningStrategy;
import gate.creole.ConditionalSerialAnalyserController;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.RunningStrategy;
import gate.util.InvalidOffsetException;

/**
 * Executes the entire preprocessing layer. The class needs a pool of configured
 * GATE pipelines. 
 * 
 * @author "Julian Müller"
 * 
 */
public class Preprocessor {

	@Inject
	ObjectPool<ConditionalSerialAnalyserController> pool;

	Configuration configuration;

	public Query preprocess(String queryInputString)
			throws InvalidOffsetException, ResourceInstantiationException,
			ExecutionException, JsonProcessingException {
		Query query = null;

		ConditionalSerialAnalyserController annieController = pool
				.borrowObject();

		try {
			// Change whether the pr should run or not.
			for (Object res : annieController.getRunningStrategies()) {
				AnalyserRunningStrategy pr = (AnalyserRunningStrategy) res;
				pr.setRunMode(RunningStrategy.RUN_ALWAYS);
				
				String resourceName = pr.getPR().getName();
				Boolean shouldRun = configuration.getWhichPRShouldrun().get(
						resourceName);

				if (shouldRun != null && !shouldRun)
					pr.setRunMode(RunningStrategy.RUN_NEVER);
			}

			// Set the corpus/document for the query.
			Corpus corpus = (Corpus) Factory
					.createResource("gate.corpora.CorpusImpl");

			FeatureMap params = Factory.newFeatureMap();
			params.put("stringContent", queryInputString);

			Document doc = (Document) Factory.createResource(
					"gate.corpora.DocumentImpl", params);
			corpus.add(doc);

			annieController.setCorpus(corpus);
			annieController.execute();

			query = new GATEDocumentAdapter(doc);

		} finally {
			// ensures the return of the expensive pipeline.
			pool.returnObject(annieController);
		}

		return query;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
}
