package de.htwg.iqes.global;

import gate.Gate;

import java.io.File;

import play.Application;
import play.Configuration;
import play.GlobalSettings;
import play.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.htwg.iqes.global.di.modules.GuiceModule;

/**
 * The global class of a Play! application has two purposes:
 * 
 * <ul>
 * <li>Bootstrap the necessary components</li>
 * <li>Create controllers and inject instances delivered by the dependeny
 * injection</li>
 * </ul>
 * 
 * @author "Julian Müller"
 * 
 */
public class Global extends GlobalSettings {

	private static final String GATE_NAME_PREFIX = "gate.";

	private static final String GATE_PATH_NAME = GATE_NAME_PREFIX + "path";

	private static final String GATE_PLUGIN_NAME = GATE_NAME_PREFIX
			+ "pluginPath";

	private static final String GATE_SITE_CONFIG_NAME = GATE_NAME_PREFIX
			+ "siteConfigPath";

	private static final String GATE_USER_CONFIG_NAME = GATE_NAME_PREFIX
			+ "userConfigPath";

	private Injector injector;

	@Override
	public <A> A getControllerInstance(Class<A> controllerClass)
			throws Exception {
		return injector.getInstance(controllerClass);
	}

	/**
	 * Gets executed on the start of IQES.
	 */
	public void onStart(Application app) {
		Configuration configs = app.configuration();
		File absolutPathToPlay = app.path().getAbsoluteFile();

		String gatePath = absolutPathToPlay + configs.getString(GATE_PATH_NAME);
		String pluginPath = configs.getString(GATE_PLUGIN_NAME);
		String siteConfig = configs.getString(GATE_SITE_CONFIG_NAME);
		String userConfig = configs.getString(GATE_USER_CONFIG_NAME);

		initializeGateAppIfNotAlreadyRunning(gatePath, pluginPath, siteConfig,
				userConfig);

		initializeGuiceInjector(app);
	}

	/**
	 * Initialize the injector Module.
	 * 
	 * @param app
	 */
	private void initializeGuiceInjector(Application app) {
		injector = Guice.createInjector(new GuiceModule(app));
	}

	/**
	 * Initialize the gate module. Hint: the initialization only gets executed
	 * if the module has not been initialized already.
	 * 
	 * @param gatePath
	 * @param pluginPath
	 * @param siteConfig
	 * @param userConfig
	 */
	public void initializeGateAppIfNotAlreadyRunning(String gatePath,
			String pluginPath, String siteConfig, String userConfig) {
		if (!Gate.isInitialised()) {
			initializeGateApp(gatePath, pluginPath, siteConfig, userConfig);
		}
	}

	/**
	 * Final gate initializer.
	 * 
	 * @param gatePath
	 * @param pluginPath
	 * @param siteConfig
	 * @param userConfig
	 */
	public void initializeGateApp(String gatePath, String pluginPath,
			String siteConfig, String userConfig) {
		Logger.info("Initialize Gate Environment");

		File gateHome = new File(gatePath);

		Gate.setGateHome(gateHome);
		Gate.setSiteConfigFile(new File(gatePath, siteConfig));
		Gate.setUserConfigFile(new File(gatePath, userConfig));

		try {
			Gate.init();
		} catch (Exception e) {
			throw new GateInitializationException(
					"Initialization of Gate wasn't successfull with the following parameters:"
							+ "\nGateHome : " + gateHome
							+ "\nSiteConfigFile : " + siteConfig
							+ "\nUserConfigFile : " + userConfig, e);
		}

		Logger.info("Gate started");
	}

	public static class GateInitializationException extends RuntimeException {

		private static final long serialVersionUID = 384349463132110126L;

		public GateInitializationException() {
			super();
		}

		public GateInitializationException(String message) {
			super(message);
		}

		public GateInitializationException(String message, Throwable cause) {
			super(message, cause);
		}

		public GateInitializationException(Throwable cause) {
			super(cause);
		}
	}
}
