package de.htwg.iqes.global.di.modules;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import gate.Corpus;
import gate.Factory;
import gate.creole.ConditionalSerialAnalyserController;
import gate.creole.ResourceInstantiationException;
import gate.persist.PersistenceException;
import gate.util.persistence.PersistenceManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Singleton;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.IOUtils;

import play.Application;
import play.libs.Akka;
import akka.actor.ActorSystem;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import com.google.inject.multibindings.Multibinder;

import de.htwg.iqes.domain.configuration.Configuration;
import de.htwg.iqes.domain.configuration.ConfigurationFactory;
import de.htwg.iqes.domain.configuration.impl.ConfigurationFactoryImpl;
import de.htwg.iqes.domain.configuration.impl.ConfigurationImpl;
import de.htwg.iqes.queryexpansion.GeoNamesQE;
import de.htwg.iqes.queryexpansion.GermaNetQE;
import de.htwg.iqes.queryexpansion.QueryExpansion;
import de.htwg.iqes.queryexpansion.dbpedia.DBpediaQE;
import de.htwg.iqes.queryexpansion.dbpedia.modules.DBpediaExpandClassQE;
import de.htwg.iqes.queryexpansion.dbpedia.modules.DBpediaExpandCompanyProductsByCategoryQE;
import de.htwg.iqes.queryexpansion.dbpedia.modules.DBpediaExpandCompanyProductsQE;
import de.htwg.iqes.queryexpansion.dbpedia.modules.DBpediaExpandMusicGenresQE;
import de.tuebingen.uni.sfs.germanet.api.GermaNet;
import easypool.ObjectPool;

/**
 * The main Guice module of IQES.
 * 
 * @author "Julian Müller"
 *
 */
public class GuiceModule extends AbstractModule {

	private static final String GATE_NAME_PREFIX = "gate.";

	private static final String GATE_IQES_PIPELINE_PATH_NAME = GATE_NAME_PREFIX
			+ "iqespipelinePath";

	private static final String GERMANET_PATH_NAME = "germanet.path";

	private static final String GATE_CORPORA_CORPUS_IMPL_CLASS_PATH = "gate.corpora.CorpusImpl";

	private static final String IQES_DEFAULT_CONFIGURATION_PATH_NAME = "defaultConfigurationPath";

	private play.Configuration config;

	private Application app;

	public GuiceModule(Application app) {
		this.app = app;
		this.config = app.configuration();
	}

	@Override
	protected void configure() {
		bind(ConfigurationFactory.class).to(ConfigurationFactoryImpl.class);

		// Multibinder for QE Modules
		Multibinder<QueryExpansion> qeBinder = Multibinder.newSetBinder(
				binder(), QueryExpansion.class);
		qeBinder.addBinding().to(GeoNamesQE.class);
		qeBinder.addBinding().to(GermaNetQE.class);
		qeBinder.addBinding().to(DBpediaQE.class);

		// The multibinder for fine granular DBpedia QE Modules
		Multibinder<QueryExpansion> qeBinderForDBpedia = Multibinder
				.newSetBinder(binder(), QueryExpansion.class,
						DBpediaQueryExpansion.class);
		qeBinderForDBpedia.addBinding()
				.to(DBpediaExpandCompanyProductsQE.class);
		qeBinderForDBpedia.addBinding().to(
				DBpediaExpandCompanyProductsByCategoryQE.class);
		qeBinderForDBpedia.addBinding().to(DBpediaExpandMusicGenresQE.class);
		qeBinderForDBpedia.addBinding().to(DBpediaExpandClassQE.class);
	}

	@Retention(RUNTIME)
	@Target({ FIELD, PARAMETER, METHOD })
	@BindingAnnotation
	public static @interface DBpediaQueryExpansion {
	}

	@Provides
	Corpus providesCorpusImpl() throws ResourceInstantiationException {
		return (Corpus) Factory
				.createResource(GATE_CORPORA_CORPUS_IMPL_CLASS_PATH);
	}

	@Provides
	ObjectMapper providesObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new GuavaModule());

		return mapper;
	}

	@Provides
	@Singleton
	GermaNet provideGermaNet() throws FileNotFoundException,
			XMLStreamException, IOException {
		File germaNetFileDirectory = new File(app.path()
				+ config.getString(GERMANET_PATH_NAME));
		GermaNet gnet = null;

		gnet = new GermaNet(germaNetFileDirectory, true);

		return gnet;
	}

	@Provides
	@Singleton
	ObjectPool<ConditionalSerialAnalyserController> provideSerialAnalyserPool() {
		final int minIdle = config.getInt("preprocessingPool.minSize");
		final int maxIdle = config.getInt("preprocessingPool.maxSize");
		final long validationInterval = config
				.getLong("preprocessingPool.validationInterval");

		return new ObjectPool<ConditionalSerialAnalyserController>(minIdle,
				maxIdle, validationInterval) {

			@Override
			protected ConditionalSerialAnalyserController createObject() {
				ConditionalSerialAnalyserController controller = null;

				try {
					controller = (ConditionalSerialAnalyserController) PersistenceManager
							.loadObjectFromFile(new File(config
									.getString(GATE_IQES_PIPELINE_PATH_NAME)));
				} catch (PersistenceException e) {
					e.printStackTrace();
				} catch (ResourceInstantiationException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return controller;
			}

		};
	}

	@Provides
	ActorSystem providesAkkaSystem() {
		return Akka.system();
	}

	@Provides
	Configuration providesConfiguration() throws JsonParseException,
			JsonMappingException, IOException {
		StringWriter writer = new StringWriter();
		File file = new File(
				config.getString(IQES_DEFAULT_CONFIGURATION_PATH_NAME));
		InputStream in = new FileInputStream(file);

		try {
			IOUtils.copy(in, writer, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}

		ObjectMapper mapper = new ObjectMapper();

		return mapper.readValue(writer.toString(), ConfigurationImpl.class);
	}

}
