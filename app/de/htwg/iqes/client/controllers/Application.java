package de.htwg.iqes.client.controllers;

import javax.inject.Inject;

import play.Routes;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;

import de.htwg.iqes.controller.IQESController;
import de.htwg.iqes.domain.QueryForm;
import de.htwg.iqes.queryexpansion.domain.Query;

/**
 * The Application class is the REST specific controller of the IQES System. The
 * main dependencies gets injected by the configured global class.
 * 
 * @author "Julian Müller"
 * 
 */
public class Application extends Controller {

	@Inject
	IQESController iqes;

	@Inject
	ObjectMapper mapper;

	private static Form<QueryForm> queryForm = Form.form(QueryForm.class);

	/**
	 * Visualize the Frontpage of IQES.
	 */
	public static Result index() {
		return ok(de.htwg.iqes.client.views.html.index.render());
	}

	/**
	 * The query methode visualize the entire Query Expansion possibilities of
	 * IQES. If the incoming request has got a session attached with a 'query'
	 * key the methode tries to bind the saved key-value-pairs. Otherwise a form
	 * with a default configuration will be shown.
	 */
	public static Result query() {
		Form<QueryForm> filledForm = null;

		if (session().containsKey("query")) {
			// Initialize form with already saved form.
			filledForm = queryForm.bind(session());
		} else {
			// Initialize form with default values.
			filledForm = queryForm.fill(new QueryForm());
		}

		return ok(de.htwg.iqes.client.views.html.queryform.render(filledForm));
	}

	/**
	 * Expands Queries which are delivered with the request methode POST.
	 */
	public Result expandPOST() throws JsonProcessingException {
		Form<QueryForm> form = queryForm.bindFromRequest();
		session().putAll(form.data());

		if (form.hasErrors()) {
			// Errors! ---> return the form with error messages.
			return ok(de.htwg.iqes.client.views.html.queryform.render(form));
		} else {
			QueryForm query = form.get();

			// Main Action! Expand a query.
			Query expandedQuery = iqes.expandQuery(query.query, query);

			return ok(mapper.writeValueAsString(expandedQuery));
		}
	}

	/**
	 * Expands Queries which are delivered with the request methode GET.
	 */
	public Result expandGET(String query) throws JsonProcessingException {
		Optional<String> configJsonString = Optional.fromNullable(request()
				.getQueryString("config"));

		// Main Action! Expand a query.
		Query expandedQuery = iqes.expandQuery(query, configJsonString);

		return ok(mapper.writeValueAsString(expandedQuery));
	}

	/**
	 * Preprocesses Queries which are delivered with the request methode GET.
	 */
	public Result preprocess(String query) throws JsonProcessingException {
		Optional<String> configJsonString = Optional.fromNullable(request()
				.getQueryString("config"));

		Query preprocessedQuery = iqes.preprocess(query, configJsonString);

		return ok(mapper.writeValueAsString(preprocessedQuery));
	}

	/**
	 * Javascript Router to use the routes out of Javascript.
	 */
	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");
		return ok(Routes.javascriptRouter("jsRoutes",
				de.htwg.iqes.client.controllers.routes.javascript.Application
						.preprocess(),
				de.htwg.iqes.client.controllers.routes.javascript.Application
						.expandGET(),
				de.htwg.iqes.client.controllers.routes.javascript.Application
						.query(),
				de.htwg.iqes.client.controllers.routes.javascript.Application
						.index(),
				de.htwg.iqes.client.controllers.routes.javascript.Application
						.expandPOST()));
	}

}
