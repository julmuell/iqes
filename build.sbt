name := "iqes"

version := "1.0-SNAPSHOT"

val gate = "uk.ac.gate" % "gate-core" % "7.1"

val guava = "com.google.guava" % "guava" % "15.0"

val jena =  "org.apache.jena" % "apache-jena-libs" % "2.10.0" excludeAll( ExclusionRule(organization = "org.slf4j") )

val pellet = "com.github.ansell.pellet" % "pellet-jena" % "2.3.4-ansell" excludeAll( ExclusionRule(organization = "org.slf4j") )
       
val guice = "com.google.inject" % "guice" % "3.0"

val guiceMultibinder = "com.google.inject.extensions" % "guice-multibindings" % "3.0"

val guiceJacksonDataBinder = "com.fasterxml.jackson.datatype" % "jackson-datatype-guava" % "2.2.3"

val mockito = "org.mockito" % "mockito-all" % "1.9.5" % "test"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  gate withSources(),
  guava,
  jena,
  pellet,
  guice,
  mockito,
  guiceMultibinder,
  guiceJacksonDataBinder
)     

play.Project.playJavaSettings

net.virtualvoid.sbt.graph.Plugin.graphSettings

javaOptions += "-Dcom.sun.management.jmxremote"
